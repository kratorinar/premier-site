const scrollfunc = () => {
    let y = window.scrollY;

    if (y > 0) {

        document.querySelector('header').style.animation = 'reduce 0.5s forwards'
    } else {

        document.querySelector('header').style.animation = 'enlarge 0.5s forwards'
    }
}
window.addEventListener('scroll', scrollfunc);